class CreatePublications < ActiveRecord::Migration[6.0]
  def change
    create_table :publications do |t|
      t.string :title
      t.text :abstract
      t.text :category
      t.references :author, null: false, foreign_key: { to_table: :users }

      t.timestamps
    end
  end
end
