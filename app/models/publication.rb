class Publication < ApplicationRecord
  belongs_to :user, foreign_key: 'author_id'

  validates :title, presence: true

  def categories
    category.split(',')
  end
end
