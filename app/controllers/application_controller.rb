class ApplicationController < ActionController::Base
  before_action :clear_flashes

  def clear_flashes
    flash[:notice] = flash[:alert] = []
  end
end
