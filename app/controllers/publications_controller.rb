class PublicationsController < ApplicationController
  before_action :set_publication, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!, only: [:new, :edit, :create, :update, :destroy]

  def index
    @publications = Publication.all
  end

  def show; end

  def new
    @publication = Publication.new
  end

  def edit; end

  def create
    @publication = Publication.new(publication_params.merge(author_id: current_user.id))

    if @publication.save
      redirect_to publications_path, notice: 'Publication was successfully created.'
    else
      flash[:alert] = @publication.errors.full_messages
      render :new
    end
  end

  def update
    if @publication.update(publication_params)
      redirect_to publications_path
    else
      render :edit
    end
  end

  def destroy
    @publication.destroy
    redirect_to publications_url, notice: 'Publication was successfully destroyed.'
  end

  private

  def set_publication
    @publication = Publication.find(params[:id])
  end

  def publication_params
    params.require(:publication).permit(:title, :abstract, :category)
  end
end
